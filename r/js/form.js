// -----------------
// Simple Form/Modal
// Author: mozacodex@gmail.com
// ----------------------------------

!function(root) {
	"use strict";

	var APP = root.APP || (
		root.APP = {}
	)

	var current

	var option = {
		cnhead: 'form-head',
		cnbody: 'form-body',
		cnfoot: 'form-foot',
		cntitle: 'form-title',
		cnclose: 'btn-close',
		cnform: 'form-object',
		hide: function(f) {
			f.context.style.display = 'none'
		},
		show: function(f) {
			f.context.style.display = 'block'
		}
	}

	var el = function(q, p) {
		if (q && q.nodeType) return q
		p || (p = document)
		return p && p.querySelectorAll && p.querySelectorAll(q)[0] 
	}

	var equip = function(object, eq) {
		object = Object(object)
		for (var i in eq) {
			object[i] || (object[i] = eq[i])
		}
		return object
	}

	var actions = {
		'form-hide': function(ref) {
			var target = ref.getAttribute('v-target')
			if (target = el(target)) {
				target.formObject && target.formObject.hide()
			}
		},

		'form-show': function(ref) {
			var target = ref.getAttribute('v-target')
			if (target = el(target)) {
				target.formObject && target.formObject.show()
			}
		}
	}

//	var event = function() {}

	var flexize = function(form) {
		var bd = form.context
		var f = form.form

		if (!(bd && f)) return form

		var s = f.style
		if (form.body) form.body.style.height = 'auto'
		s.width = s.height = 'auto'
		var dw = Number(f.getAttribute('form-width'))
		var dh = Number(f.getAttribute('form-height'))
		var fw = dw || f.offsetWidth
		var fh = dh || f.offsetHeight

		if (bd.offsetWidth < fw) {
			s.width = '100%'
			s.left = 0
		}
		else {
			s.width = dw ? (dw.indexOf('%')!==-1? dw : dw + 'px') : 'auto'
			s.left = ((bd.offsetWidth - f.offsetWidth)/2) + 'px'
		}

		if (bd.offsetHeight < fh) {
			s.height = '100%'
			s.top = 0
		}
		else {
			s.height = dh ? (dh.indexOf('%')!==-1? dh : dh + 'px') : 'auto'
			s.top = ((bd.offsetHeight - f.offsetHeight)/2) +  'px'
		}

		if (form.body) {
			fh = f.offsetHeight
			if (form.head) fh -= form.head.offsetHeight
			if (form.foot) fh -= form.foot.offsetHeight
			form.body.style.height = fh + 'px'
		}

		return form
	}

	var Form = function(context, options) {
		var f = this
		if (context = el(context)) {
			if (context.formObject) return context.formObject
			var opt = (f.cf = equip(options, option))
			f.context = context
			f.form = el('.' + opt.cnform, context)
			f.head = el('.' + opt.cnhead, context)
			f.body = el('.' + opt.cnbody, context)
			f.foot = el('.' + opt.cnfoot, context)
			f.caption = el('.' + opt.cntitle, context)
			context.formObject = f
			this.hide()
		}
	}

	Form.prototype = {
		hide: function() {
			this.context && this.cf.hide(this)
			return this
		},

		show: function() {
			if (this.context) {
				this.cf.show(this)
				this.adjustSize()
				current = this
			}
			return this
		},

		adjustSize: function() {
			return flexize(this)
		},

		content: function(content) {
			var body = this.body
			if (!arguments.length)
				return body && body.innerHTML || ""
			if (content) {
				if (content.nodeType) {
					body.appendChild(content)
				} else body.innerHTML = content
			}
			return this
		},

		title: function(title) {
			var t = this.caption
			if (!arguments.length)
				return t && t.innerHTML || ""
			this.caption.innerHTML = title
			return this
		}
	}

	document.addEventListener('click', function(e) {
		var v, ref = e.target
		if (v = ref && ref.getAttribute('v-action')) {
			actions[v] && actions[v](ref)
		}
	})

	document.addEventListener('keyup', function(e) {
		e.keyCode === 27 && current && current.hide()
	})

	var timeout
	root.addEventListener('resize', function() {
		clearTimeout(timeout)
		timeout = setTimeout(function() {
			current && flexize(current)
		}, 300)
	});

	APP.form = function(context, options) {
		return new Form(context, options)
	}

	Form.defopts = option
	APP.attracts = actions
	APP.el = el
	APP.Form = Form
}(this)

