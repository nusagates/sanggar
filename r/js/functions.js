/* 
 * Name: functions.js
 * Author: Ahmad Budairi
 * Author_URL: www.nusagates.com
 */
function fetch_data(tableID, url) {
    var tabel = $(tableID).DataTable( {
        "processing": true,
        "paging": true,
        "serverSide": true,
        "ajax":{ 
           "url": url,
           "type": "post"
        }
    } );
 
}
function add_data(url, data, infoBoxId, modalId, tabelId) {
    $('' + infoBoxId + '').hide();
    $.ajax({
        url: url,
        method: 'post',
        data: data,
        success: function (s) {
            $('' + infoBoxId + '').slideDown();
            $('' + infoBoxId + '').text(s);
            if (s === '1') {
                $('' + infoBoxId + '').hide();
                $('' + modalId + '').modal("hide");
                $('' + tabelId + '').DataTable().ajax.reload();
                $("#form-tambah").trigger('reset'); 
            }
        }
    });
}

function edit_form(url, modalId, data, container) {
    $("" + modalId + "").modal("show");
    $.ajax({
        url: url,
        method: 'post',
        data: {id: data},
        success: function (s) {
            $("" + container + "").html(s);
        }
    });
}

function update_data(url, data, infoBoxId, modalId, tabelId) {
    $.ajax({
        url: url,
        method: 'post',
        data: data,
        success: function (s) {
            $('' + infoBoxId + '').slideDown();
            $('' + infoBoxId + '').text(s);
            if (s === '1') {
                $('' + infoBoxId + '').text('memroses');
                $('' + modalId + '').modal("hide");
                $('' + tabelId + '').DataTable().ajax.reload();
            }
        }
    });
}

function delete_form(url, modalId, data, container) {
    $("" + modalId + "").modal("show");
    $.ajax({
        url: url,
        method: 'post',
        data: {id: data},
        success: function (s) {
            $("" + container + "").html(s);
        }
    });
}

function remove(url, data, modalId, tabelId) {
    $.ajax({
        url: url,
        method: 'post',
        data: data,
        success: function (s) {
            if (s === '1') {
                $('' + modalId + '').modal("hide");
                $('' + tabelId + '').DataTable().ajax.reload();
            }
        }
    });
}
$.datetimepicker.setLocale('id');
