<?php

class page_gate {

 public $ctx;
 private $table = 'kategori';
 private $id = 'kat_id';

 function __construct($ctx) {
  $this->ctx = $ctx;
 }

 /**
  * fungsi untuk menampilkan halaman index kategori
  */
 function index() {
  $this->ctx->_load_template($this, 'gate');
 }
 
 function login(){
  if (General::s_post('username', $username))exit(text('required', 'Telpon/Email'));
  if (General::s_post('password', $password))exit(text('required', 'Password'));
  $db = $this->ctx->db;
  $query= $db->prepare("select * from users where phone=? or email=?");
  $query->execute(array($username, $username));
  if($query->rowCount()){
  $col = $query->fetchObject();
   if(password_verify($password, $col->password)){
   $_SESSION['logged-in'] = true;
   $_SESSION['user_id']=$col->id;
   $_SESSION['user_name']=$col->fullname;
   $_SESSION['role']=$col->role;
   echo "1";
   exit;
  }else{
   echo "Password tidak cocok.";
  }
  }else{
   echo "Pengguna tidak ditemukan";
  }
 }
 
 function logout(){
  session_unset();
  session_destroy();
  header("location:".$this->ctx->base_url."/gate");
 }

}
