<?php

class page_report {

 public $ctx;
 private $table = 'criteria';
 private $id = 'id';

 function __construct($ctx) {
  if (!$ctx->isUserLoggedIn()) {
   $ctx->_route('gate');
   exit;
  }
  $this->ctx = $ctx;
 }

 /**
  * fungsi untuk menampilkan halaman index kategori
  */
 function index() {
  if (!$this->ctx->isUserLoggedIn()) {
   $this->ctx->_route('gate');
   exit;
  } else {
   $this->ctx->_load_template($this, 'report');
  }
 }

 function read() {
  if (General::s_post('month', $month))
   exit(text('required_select', 'Bulan'));
  $query = $this->ctx->db->prepare("SELECT r.id, r.tanggal, r.user, e.name, GROUP_CONCAT(c.name order by r.criteria) kriteria, GROUP_CONCAT(c.weight order by r.criteria) bobot, GROUP_CONCAT(r.value order by r.criteria) nilai
FROM `reports` r
LEFT JOIN employers e 
ON r.user=e.id
LEFT JOIN criteria c 
ON r.criteria=c.id
 WHERE month(r.tanggal) =? GROUP by r.user");
  $query->execute(array($month));
  if ($query->rowCount()) {
   $kriteria = explode(",", $query->fetchObject()->kriteria);
   while ($col = $query->fetchObject()) {
    $nilai[] = explode(",", $col->nilai);
    $tanggal[] = array($col->tanggal);
    $nama[] = array($col->name);
    $id[] = array($col->id);
    $users[] = array($col->user);
    $total[] = General::totalValue($this->ctx->db, $col->user);
   }
   echo json_encode(array(
       "kriteria" => $kriteria,
       "nilai" => $nilai,
       "tanggal" => $tanggal,
       "nama" => $nama,
       "id" => $id,
       "user" => $users,
       "total" => $total
   ));
  }
 }

 /**
  * Fungsi ini digunakan untuk menambah kategori baru
  * 
  */
 function pdf() {
  if (General::s_post('bulan', $month))
   exit(text('required_select', 'Bulan'));
  $query = $this->ctx->db->prepare("SELECT year(r.tanggal) tahun, r.id, r.tanggal, r.user, e.name, GROUP_CONCAT(c.name order by r.criteria) kriteria, GROUP_CONCAT(c.weight order by r.criteria) bobot, GROUP_CONCAT(r.value order by r.criteria) nilai
FROM `reports` r
LEFT JOIN employers e 
ON r.user=e.id
LEFT JOIN criteria c 
ON r.criteria=c.id
 WHERE month(r.tanggal) =? GROUP by r.user");
  $query->execute(array($month));
  $tahun = "";
  if ($query->rowCount()) {
   $kriteria = explode(",", $query->fetchObject()->kriteria);
   $tahun = $query->fetchObject()->tahun;
  }
  $criteria_count = count($kriteria);
  
  $title = "Penilaian Karyawan Bulan ".General::namaBulan($month)." ".$tahun;
  $pdf = new TCPDF("L", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  $pdf->SetHeaderData('\logo.jpg', 20, HEADER_TITLE_1, HEADER_TITLE_2 . "\n" . HEADER_TITLE_3, array(0, 0, 0), array(0, 64, 128));
  $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 14));
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('Edumin Indonesia');
  $pdf->SetTitle('Laporan Kinerja Pegawai');
  $pdf->SetSubject('Administrasi Pegawai');
  $pdf->SetKeywords('Laporan Pegawai, Administrasi Pegawai');

  $pdf->AddPage();
  $html = '<style>
           table, td, th{border: 1px solid #eee; padding: 2px; text-align:center;vertical-align:center }
           .document-title{text-align:center;margin-top:20px;padding-bottom:10px}
          .document-title div{font-size: 14pt; text-decoration: underline}
          .document-title span{font-size: 10pt;}
          .foot, .head{background-color: #eff; vertical-align: center}
           </style>
           <div class="document-title">
          <div>'.$title.'</div></div>
          <div style="clear:both"></div>
          <table>
          <thead>
          <tr class="head">
           <th rowspan="2">No.</th>
           <th rowspan="2">Nama Karyawan</th>
           <th colspan="' . $criteria_count . '">Kriteria</th>
           <th rowspan="2"> <br> Total</th>
          </tr>
          <tr class="head">';
            for ($i = 0; $i < count($kriteria); $i++) {
             $html .="<th>$kriteria[$i]</th> \n";
            }
            
        $html .='
          </tr>
          </thead>
          <tbody>';
  while ($col = $query->fetchObject()) {
   $nilai[] = explode(",", $col->nilai);
   $tanggal[] = array($col->tanggal);
   $nama[] = array($col->name);
   $users[] = array($col->user);
  }
  $k = 1;
  for ($i = 0; $i < count($tanggal); $i++) {
   $name = $nama[$i][0];
   $tgl = $tanggal[$i][0];
   $user = $users[$i][0];
   $nn = $nilai[$i];
   $html .= "<tr>"
           . "<td>$k</td>"
           . "<td>$name</td>";
   foreach ($nn as $g) {
    $html .= "<td>" . $g . "</td>";
   }
   $html .= "<td>" . General::totalValue($this->ctx->db, $user) . "</td>"
           . "</tr>";
   $k++;
  }

  //print_r($nilai);
  $html .= "<br>";
  //print_r($nilai);
  $html .= '</tbody>
          </table>';
  $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
  $pdf->Output($title . '.pdf', 'I');
  //echo $html;
 }

}
