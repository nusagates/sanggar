<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Class ini digunakan untuk menghandel fungsi-fungsi umum yang dibutuhkan aplikasi
 *
 * @author nusagates <nusagates@gmail.com>
 * @category fungsi
 * @version 1.0
 */
class General {

 public static function sidebar_menu($ctx) {
 
    ?>
    <ul class="sidebar-menu">
   <li class="header text-center">NAVIGASI UTAMA</li>
     <li<?php echo $ctx->req_uri=="" ||$ctx->req_uri=="criteria" ?" class='active' ":'' ?>><a href="<?php echo $ctx->base_url . "/criteria" ?>"><i class="fa fa-circle-o text-aqua"></i> Kriteria Penilaian</a></li>
     <li<?php echo $ctx->req_uri=="assessment"?" class='active' ":'' ?>><a href="<?php echo $ctx->base_url . "/assessment" ?>"><i class="fa fa-circle-o text-aqua"></i> Buat Penilaian</a></li>
     <li<?php echo $ctx->req_uri=="report"?" class='active' ":'' ?>><a href="<?php echo $ctx->base_url . "/report" ?>"><i class="fa fa-circle-o text-aqua"></i> Rekap Data</a></li>

  </ul>
  <?php

 }

 /**
  * Fungsi ini digunakan untuk membuat variabel dari $_POST dan mengambil nilainya
  * @param string $label diisi dengan argumen/key $_POST atau value dari atribut name suatu input
  * @param variable $var nama variabelyang mau digunakan untuk mewakili $_POST
  * @return string value atau nilai dari $_POST
  */
 public static function s_post($label, &$var = null) {
  return empty($_POST[$label]) || '' === ($var = trim($_POST[$label]));
 }

 public static function menu($ctx, $key) {
  if ($ctx->req_uri == $key) {
   echo '<li class="active"><a href="' . $ctx->base_url . "/" . $key . '"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>';
  } else {
   echo '<li ><a href="../../index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>';
  }
 }

 public static function html_input($id, $label, $col = '6', $value = '', $required = '0', $type = 'text', $attr='') {
  $req = $required == "1" ? "required='required'" : '';
  ?>
  <div class="col-md-<?php echo $col ?>">
   <div class="form-group">
    <label for="<?php echo $id ?>"><?php echo $label ?></label>
    <input <?php echo $attr ?> <?php echo $req ?> value="<?php echo $value ?>" type="<?php echo $type ?>" class="form-control" name="<?php echo $id ?>" id="<?php echo $id ?>"/>
   </div>
  </div>
  <?php
 }

 public static function html_input_hidden($id, $value = '') {
  ?>
  <input required="required" value="<?php echo $value ?> " type="hidden"  name="<?php echo $id ?>" id="<?php echo $id ?>"/>
  <?php
 }

 public static function html_checkbox($id, $label, $col = '6', $checked = '0') {
  $check = $checked == '1' ? "checked='checked'" : '';
  ?>
  <div class="col-md-<?php echo $col ?>">
   <div class="form-group">
    <label><input <?php echo $check ?> name="<?php echo $id ?>" id="<?php echo $id ?>" type="checkbox"/> <?php echo $label ?></label>
   </div>
  </div>
  <?php
 }

 public static function html_textarea($id, $label, $col = '6', $value = '', $required = '0') {
  $req = $required == "1" ? "recuired='required'" : '';
  ?>
  <div class="col-md-<?php echo $col ?>">
   <div class="form-group">
    <label for="<?php echo $id ?>"><?php echo $label ?></label>
    <textarea <?php echo $req ?>  class="form-control" name="<?php echo $id ?>" id="<?php echo $id ?>"><?php echo $value ?> </textarea>
   </div>
  </div>
  <?php
 }

 public static function html_info($text = 'Silahkan isi data-data yang diperlukan.') {
  ?>
  <div class="col-md-12">
   <div class="text-info"><i class="fa fa-exclamation-circle"></i> <span class="info-text"><?php echo $text ?></span></div>
  </div>
  <?php
 }


 public static function html_modal_tambah() {
  ?>
  <div id="modal-tambah" class="modal fade" role="dialog" >
   <div class="modal-dialog">
    <div class="modal-content">
     <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Tambah</h4>
     </div>
     <div class="modal-body">
      <div class="row">
       <form id="form-tambah" method="post">
  <?php
  General::html_input("nama", "Nama Kategori", 12, '', 1);
  General::html_textarea("keterangan", "Keterangan", 12, '');
  General::html_info();
  ?>

       </form>
      </div>
     </div>
     <div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      <button id="btn-tambah" type="button" class="btn btn-primary">Tambah</button>
     </div>
    </div>
   </div>
  </div>
  <?php
 }

 public static function html_modal_edit($mdl_id='modal-edit',$mdl_type="", $btn_id='btn-update', $title='Edit') {
  ?>
  <div id="<?php echo $mdl_id ?>" class="modal fade" role="dialog" >
   <div class="modal-dialog <?php echo $mdl_type ?>">
    <div class="modal-content">
     <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title"><?php echo $title; ?></h4>
     </div>
     <div class="modal-body">
      <div class="row">
       <div id="edit-form-container">
        <div class="col-md-12 text-center">
         <h2 class="fa fa-spin fa-spinner" style="font-size: 36px"></h2>
        </div>
       </div>
      </div>
     </div>
     <div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      <button id="<?php echo $btn_id ?>" type="button" class="btn btn-primary">Update</button>
     </div>
    </div>
   </div>
  </div>
  <?php
 }

 public static function html_modal_hapus() {
  ?>
  <div id="modal-hapus" class="modal fade" role="dialog" >
   <div class="modal-dialog">
    <div class="modal-content">
     <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Hapus</h4>
     </div>
     <div class="modal-body">
      <div class="row">
       <div id="hapus-form-container">
        <div class="col-md-12 text-center">
         <h2 class="fa fa-spin fa-spinner" style="font-size: 36px"></h2>
        </div>
       </div>
      </div>
     </div>
     <div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
      <button id="btn-delete" type="button" class="btn btn-danger">Hapus</button>
     </div>
    </div>
   </div>
  </div>
  <?php
 }

 public static function error_PDO($ctx) {
  print_r($ctx->errorInfo());
 }
 
 public static function intohex($dec) {
  $hex = '';
  do {
    $last = bcmod($dec, 16);
    $hex = dechex($last) . $hex;
    $dec = bcdiv(bcsub($dec, $last), 16);
  } while ($dec > 0);
  return $hex;
}

public static function totalValue($db, $id){
 $query = $db->prepare("SELECT SUM(value) total FROM `reports` WHERE user=?");
 $query->execute(array($id));
 if($query->rowCount()){
  return $query->fetchObject()->total;
 }else{
  return "0";
 }
}

 public static function namaBulan($angka) {
  $nama = "";
  switch ($angka) {
   case 1: $nama = "Januari";
    break;
   case 2: $nama = "Februari";
    break;
   case 3: $nama = "Maret";
    break;
   case 4: $nama = "April";
    break;
   case 5: $nama = "Mei";
    break;
   case 6: $nama = "Juni";
    break;
   case 7: $nama = "Juli";
    break;
   case 8: $nama = "Agustus";
    break;
   case 9: $nama = "September";
    break;
   case 10: $nama = "Oktober";
    break;
   case 11: $nama = "November";
    break;
   case 12: $nama = "Desember";
    break;
  }
  return $nama;
 }

}
