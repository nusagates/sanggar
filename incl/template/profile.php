<?php
isset($this) || exit;

$title = "Profile";

ob_start();
?>

<div class="row">
 <div class="col-md-3">

  <!-- Profile Image -->
  <div class="box box-primary">
   <div class="box-body box-profile">
    <img class="profile-user-img img-responsive img-circle" src="<?php echo User::user_image_url($this->base_url) ?>" alt="User profile picture">
    <h3 class="profile-username text-center"><?php echo User::display_name() ?></h3>
    <p class="text-muted text-center"><?php echo User::getUserRole() ?></p>

    <ul class="list-group list-group-unbordered">
     <li class="list-group-item">
      <b>Poin</b> <a class="pull-right">1,322</a>
     </li>
     <li class="list-group-item">
      <b>Hari Aktif</b> <a class="pull-right">543</a>
     </li>
     <li class="list-group-item">
      <b>Teman</b> <a class="pull-right">13,287</a>
     </li>
    </ul>
   </div><!-- /.box-body -->
  </div><!-- /.box -->

  <!-- About Me Box -->
  <div id="about-me" class="box box-primary">
   <div class="box-header with-border">
    <h3 class="box-title">Tentang Saya</h3>
   </div><!-- /.box-header -->
   <div class="box-body">
    <strong class="tbl-education"><i class="fa fa-book margin-r-5"></i>  Pendidikan</strong>
    <p class="tbl-education" class="text-muted">
     Google University
    </p>

    <hr>

    <strong><i class="fa fa-map-marker margin-r-5"></i> Alamat</strong>
    <p class="text-muted">Salatiga</p>

    <hr>

    <strong><i class="fa fa-pencil margin-r-5"></i> Hobi</strong>
    <p>
     <span class="label label-danger">Menulis</span>
     <span class="label label-success">Membaca</span>
     <span class="label label-info">Ngeblog</span>
     <span class="label label-warning">Nyekrip</span>
     <span class="label label-primary">Ngoding</span>
    </p>

    <hr>
    <strong><i class="fa fa-file-text-o margin-r-5"></i> Catatan</strong>
    <p>Horeee. Sampai halaman profil.</p>
    <a href="#" class="btn btn-info btn-block"><b>Edit</b></a>
   </div><!-- /.box-body -->
  </div><!-- /.box -->
 </div>

 <div class="col-md-3">

  <!-- Profile Image -->
  <div class="box box-primary">
   <div class="box-body box-profile">
    <img class="profile-user-img img-responsive img-circle" src="<?php echo User::user_image_url($this->base_url) ?>" alt="User profile picture">
    <h3 class="profile-username text-center"><?php echo User::display_name() ?></h3>
    <p class="text-muted text-center"><?php echo User::getUserRole() ?></p>

    <ul class="list-group list-group-unbordered">
     <li class="list-group-item">
      <b>Poin</b> <a class="pull-right">1,322</a>
     </li>
     <li class="list-group-item">
      <b>Hari Aktif</b> <a class="pull-right">543</a>
     </li>
     <li class="list-group-item">
      <b>Teman</b> <a class="pull-right">13,287</a>
     </li>
    </ul>

    <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
   </div><!-- /.box-body -->
  </div><!-- /.box -->

  <!-- About Me Box -->
  <div class="box box-primary">
   <div class="box-header with-border">
    <h3 class="box-title">Tentang Saya</h3>
   </div><!-- /.box-header -->
   <div class="box-body">
    <strong><i class="fa fa-book margin-r-5"></i>  Pendidikan</strong>
    <p class="text-muted">
     Google University
    </p>

    <hr>

    <strong><i class="fa fa-map-marker margin-r-5"></i> Alamat</strong>
    <p class="text-muted">Salatiga</p>

    <hr>

    <strong><i class="fa fa-pencil margin-r-5"></i> Hobi</strong>
    <p>
     <span class="label label-danger">Menulis</span>
     <span class="label label-success">Membaca</span>
     <span class="label label-info">Ngeblog</span>
     <span class="label label-warning">Nyekrip</span>
     <span class="label label-primary">Ngoding</span>
    </p>

    <hr>

    <strong><i class="fa fa-file-text-o margin-r-5"></i> Catatan</strong>
    <p>Horeee. Sampai halaman profil.</p>
   </div><!-- /.box-body -->
  </div><!-- /.box -->
 </div>

</div><!-- /. row -->
<?php
General::html_modal_edit("edu-modal", "modal-sm", "btn-edu", "Perbarui Pendidikan");
General::html_modal_hapus();
?>
<script>
 $(function () {
     'use strict';
     var base_url = '<?php echo $this->base_url ?>/profile';

     $("#about-me").on("click", ".tbl-education", function (e) {
         var id = "<?php echo User::getUserId() ?>";
         e.preventDefault();
         edit_form(base_url + "/edit/education/form", "#edu-modal", id, '#edu-modal #edit-form-container');
     });

     $("#btn-edu").click(function () {
         var data = $("#form-edit").serialize();
         add_data(base_url + "/update/education", data, '.info-text', '#edu-modal', '#tabel');
     });
     $("#tabel").on("click", ".btn-hapus", function (e) {
         var id = $(this).attr("data-hapus");
         e.preventDefault();
         delete_form(base_url + "/delete/form", "#modal-hapus", id, '#hapus-form-container');
     });
     $("#btn-delete").click(function () {
         var data = $("#form-hapus").serialize();
         remove(base_url + "/delete", data, '#modal-hapus', "#tabel");
     });

     $('#tanggal').datetimepicker({
         timepicker: true,
         format: 'Y-m-d H:i:s'
     });

 });
</script>

<?php
$content = ob_get_clean();
require dirname(__FILE__) . '/dashboard.php';
?>
