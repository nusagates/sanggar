<?php User::unAuthorized() ?>
<!doctype html>
<html>
 <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="stylesheet" href="<?php echo $this->base_url . "/r/" ?>css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo $this->base_url . "/r/" ?>css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo $this->base_url . "/r/" ?>bootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo $this->base_url . "/r/" ?>css/font-awesome.min.css">
  <script src="<?php echo $this->base_url . "/r/" ?>js/jQuery-2.1.4.min.js"></script>
  <script src="<?php echo $this->base_url . "/r/" ?>bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo $this->base_url . "/r/" ?>dist/js/app.min.js"></script>
  <title>Unauthorized</title>
 </head>
 <body class="hold-transition lockscreen">
    <!-- Automatic element centering -->
    <div class="lockscreen-wrapper">
      <div class="lockscreen-logo">
        <a href="../../index2.html"><b>Jegg</b>boy</a>
      </div>
      <!-- User name -->
      <div class="lockscreen-name"><?php echo User::display_name() ?></div>

      <!-- START LOCK SCREEN ITEM -->
      <div class="lockscreen-item">
        <!-- lockscreen image -->
        <div class="lockscreen-image">
          <img src="<?php echo $this->base_url . "/r/" ?>dist/img/user2-160x160.jpg" alt="User Image">
        </div>
        <!-- /.lockscreen-image -->

        <div class="lockscreen-credentials">
         Anda tidak memiliki hak akses untuk halaman ini
        </div>

      </div><!-- /.lockscreen-item -->
      <div class="help-block text-center">
       <a class="btn btn-info" href="<?php echo $this->base_url ?>/user/profile">Lihat Profil</a>
      </div>
      <div class="text-center">
        <a href="<?php echo $this->base_url ?>/gate/logout">atau login menggunakan akun lain.</a>
      </div>
      <div class="lockscreen-footer text-center">
        Copyright &copy; 2014-2015 <br>
        All rights reserved
      </div>
    </div><!-- /.center -->

  </body>
</html>