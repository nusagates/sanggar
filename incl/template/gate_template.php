<!doctype html>
<html>
 <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="stylesheet" href="<?php echo $this->base_url . "/r/" ?>css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo $this->base_url . "/r/" ?>css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo $this->base_url . "/r/" ?>bootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo $this->base_url . "/r/" ?>css/font-awesome.min.css">
  <script src="<?php echo $this->base_url . "/r/" ?>js/jQuery-2.1.4.min.js"></script>
  <script src="<?php echo $this->base_url . "/r/" ?>bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo $this->base_url . "/r/" ?>dist/js/app.min.js"></script>
  <title>Gerbang Sanggar Pelangi</title>
 </head>
 <body class="hold-transition login-page">
  <div class="login-box">
   <div class="login-logo">
    <a href=""><b>Sanggar</b> Pelangi</a>
   </div>
   <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    <form  method="post">
     <div class="form-group has-feedback"> 
      <input name="username" type="text" class="form-control" placeholder="Telpon/Email">
      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
     </div>
     <div class="form-group has-feedback">
      <input name="password" type="password" class="form-control" placeholder="Password">
      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
     </div>
     <div class="row">
      <div class="col-xs-8">
       <div class="checkbox icheck">
        <label>
         <input type="checkbox"> Ingat Saya
        </label>
       </div>
      </div><!-- /.col -->
      <div class="col-xs-4">
       <button id="submit" type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
      </div><!-- /.col -->
     </div>
    </form>
   </div>
  </div>
  <script>
  $(function(){
   'use strict';
     var base_url = '<?php echo $this->base_url ?>/gate';
   $("#submit").click(function(e){
    e.preventDefault();
    $(".login-box-msg").html("<span class='text-yellow'><i class='fa fa-spin fa-spinner'></i> sedang divalidasi</span>");
    $.ajax({
     url: base_url+"/login",
     method: "post",
     data: $('form').serialize(),
     success: function(e){
      if(e!="1"){
       $(".login-box-msg").html("<span class='text-red'><i class='fa fa-info-circle'></i> "+e+"</span>");
      }
      else{
       $(".login-box-msg").html("<span class='text-success'><i class='fa fa-spin fa-spinner'></i> Login berhasil. Silahkan tunggu.</span>");
       window.location.href="<?php echo $this->base_url ?>";
       }
     }
    });
   });
  });
  </script>
  <?php
  
  ?>
 </body>
</html>