<?php
isset($this) || exit;

$title = "Laporan Penilaian";

ob_start();
?>

<div class="row">
 <div class="col-md-12">
  <div class="box box-primary">
   <div class="box-header">
    <i class="fa fa-edit"></i>
    <h3 class="box-title">Data Masuk</h3>
    <div id="cetak" class="pull-right">
     

    </div>
   </div>
   <div class="box-body">
    <p>
     Data yang dtitampilkan pada tabel di bawah adalah penilaian pada bulan terakhir. Untuk melihat penilaian bulan sebelumnya silahkan pilih bulan kemudian klik tombol Cetak PDF.
     Hasil penghitungan nilai karyawan menggunakan rumus sebagai berikut: <br>
     <img width="400" src="<?php echo $this->base_url ?>/gambar/equation.png"/>
    </p>
    <div class="row">
     <div class="col-md-5 col-md-offset-3">
      <div class="form-group">
       
       <div class="col-md-8">
        <form method="post" >
        <select class="form-control" name="bulan" id="bulan">
        <?php
        $month = $this->db->query("SELECT month(tanggal) bulan FROM `reports` GROUP BY month(tanggal)");
        $bulan = 1;
        if($month->rowCount()){
         while($col= $month->fetchObject()){
          echo "<option value='$col->bulan'>".General::namaBulan($col->bulan)."</option>";
          $bulan = $col->bulan;
         }
        }
        ?>
        
       </select>
        </form>
       </div>
        <span class="input-group-btn">
      <button id="export-pdf" class="btn  btn-default bg-orange" type="button"><i class="fa fa-file-pdf-o"></i> Cetak PDF</button>
     </span>
      </div>
     </div>
     <div class="col-md-12">
      <table id="tabel1" class="table display table-striped" style="width:100%">
       <thead>
           <?php
           $query = $this->db->prepare("SELECT r.id, r.tanggal, r.user, e.name, GROUP_CONCAT(c.name order by r.criteria) kriteria, GROUP_CONCAT(c.weight order by r.criteria) bobot, GROUP_CONCAT(r.value order by r.criteria) nilai
FROM `reports` r
LEFT JOIN employers e 
ON r.user=e.id
LEFT JOIN criteria c 
ON r.criteria=c.id
 WHERE month(r.tanggal) =? GROUP by r.user");
           $query->execute(array($bulan));
           if ($query->rowCount()) {
            $kriteria = explode(",", $query->fetchObject()->kriteria);
           }
           ?>
        <tr>
         <th style="vertical-align: middle" rowspan="2" width="5%">No.</th>
         <th style="vertical-align: middle" rowspan="2" >Tanggal</th>
         <th style="vertical-align: middle" rowspan="2">Nama Karyawan</th>
         <th style="text-align: center" colspan="<?php echo count($kriteria) ?>">Kriteria</th>
         <th style="vertical-align: middle" rowspan="2">Total</th>

        </tr>
        <tr>
            <?php
            for ($i = 0; $i < count($kriteria); $i++) {
             echo "<th>$kriteria[$i]</th> \n";
            }
            ?>
        </tr>
       </thead>
       <tbody>
           <?php
           while ($col = $query->fetchObject()) {
            $nilai[] = explode(",", $col->nilai);
            $tanggal[] = array($col->tanggal);
            $nama[] = array($col->name);
            $id[] = array($col->id);
            $users[] = array($col->user);
           }
           $k = 1;
           for ($i = 0; $i < count($tanggal); $i++) {
            $name = $nama[$i][0];
            $tgl = $tanggal[$i][0];
            $ID = $id[$i][0];
            $user = $users[$i][0];
            $nn = $nilai[$i];
            echo "<tr>"
            . "<td>$k</td>"
            . "<td>$tgl</td>"
            . "<td>$name</td>";
            foreach ($nn as $g){
             echo "<td>".$g."</td>";
            }
            echo "<td>".General::totalValue($this->db, $user)."</td>"
            . "</tr>";
            $k++;
           }
           
          //print_r($nilai);
           echo "<br>";
           //print_r($nilai);
           ?>
       </tbody>
      </table>
     </div>
    </div>

   </div><!-- /.box-body -->
  </div><!-- /.box -->
 </div><!-- /.col -->
</div><!-- /. row -->
<?php
General::html_modal_edit("modal-edit", "modal-sm");
General::html_modal_hapus();
?>
<script>
 $(function () {
     'use strict';
     var base_url = '<?php echo $this->base_url ?>/report';
     $("#export-pdf").click(function (s) {
      s.preventDefault();
       $("form").attr("action", base_url+"/pdf")
       $("form").submit();
     });

 });
</script>

<?php
$content = ob_get_clean();
require dirname(__FILE__) . '/dashboard.php';
?>
