<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Text
 *
 * @author nusag
 */
class Text {
    public $lang_dir;
    public $lang_id;
    public $data = array();

    function __construct($lang_dir, $lang_id) {
        $this->lang_dir = $lang_dir;
        $this->lang_id = $lang_id;
        if ($lang_dir && is_file($fn = $lang_dir . '/' . $lang_id . '.php')){
            $this->data = include($fn);
        }
    }

    function _load($file){
        if (is_file($file)) {
            $this->data = include($file) + $this->data;
        }
    }

    function l($key) {
        if (isset($this->data[$key])){
            $text = $this->data[$key];
            if (func_num_args()>1) {
                $argv = func_get_args();
                $argv[0] = $text;
                $text = call_user_func_array("sprintf", $argv);
            }
        } else $text = $key;
        return $text;
    }
}





